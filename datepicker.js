// Get curent date 
const today = new Date();

const thisYear = today.getFullYear();
const thisMonth = today.getMonth() + 1;
const thisDay = today.getDay();
const thisDate = today.getDate();

const datePicker = document.getElementById('datepickerWrapper');
const dateInput = datePicker.children.namedItem('selectDate');

// Utility functions
range = (start, end) => {
    // return array of number between given start and end points

    if (start === end) {
        return [start];
    }

    return [start, ...range(start + 1, end)];
}
toggleClass = (el, className) => {
    el.classList.toggle(className);
}
removeActive = (obj) => {
    // strips the class "active" from the children of a given DOM node

    let elems = obj.target.parentNode.children;

    [].forEach.call(elems, function(el) {
        try {
            el.classList.remove("active");
        } finally {}
    });

    return true;
}

// date functions
daysInMonth = (month, year)  => {
    // return number of days in a given month

    return new Date(year, month, 0).getDate();
}
setDate = (dateInput, year, month, day) => {
    // Set the date input value to give date

    dateInput.getElementsByTagName('input')[0].value = year.toString() + '-' + month.toString().padStart(2, 0) + '-' + day.toString().padStart(2, 0);
}
updateDate = (dateInput, dateValue) => {
    // Update the date input value give one or more of year, month, day

    let currentSetDate = dateInput.getElementsByTagName('input')[0].value.split('-');
    let currentYear = dateValue.year || currentSetDate[0];
    let currentMonth = dateValue.month || currentSetDate[1];
    let currentDay = dateValue.day || currentSetDate[2];

    setDate(dateInput, currentYear, currentMonth, currentDay)
}

// DOM update functions
makeNode = ( elemetArgs ) => {
    // handler to create DOM node

    let element = document.createElement( elemetArgs.type );

    if (elemetArgs.id) {
        element.setAttribute( 'id', elemetArgs.id );
    }
    if (elemetArgs.className) {
        element.setAttribute( 'class', elemetArgs.className );
    }
    if (elemetArgs.value) {
        element.setAttribute( 'value', elemetArgs.value );
    }
    if (elemetArgs.data) {
        element.setAttribute( 'data-' + elemetArgs.data[0], elemetArgs.data[1] );
    }
    if (elemetArgs.content) {
        element.appendChild(document.createTextNode( elemetArgs.content ));
    }

    return element;
}

// Make DOM nodes for years, months, days like:
// ul class id
//     li class id data-<year/month/day>
makeYears = (yearRange, dateInput, currentYear, currentMonth, CurrentDate) => {
    // make node for year range like:
    // ul class id
    //     li class id data-year
    
    let years = [ ...range(currentYear - yearRange, currentYear + yearRange) ];

    let yearsList = makeNode({
                'type': 'ul',
                'id': 'yearSelect',
                'className': 'year_select'
            });

    years.forEach(function(year) {
        let classes = 'year';
        if (year == currentYear) {
            classes += ' active';
        }
        let el = makeNode({
                'type': 'li',
                'className': classes,
                'data': ['year', year],
                'content': year
            });
        el.addEventListener('click', event => {
            removeActive(event);

            let selectYear = event.target.attributes['data-year'].value;

            toggleClass(event.target, 'active');
            updateDate(dateInput, {'year': selectYear});
        });
        yearsList.appendChild( el );
    });

    return yearsList
}
makeMonths = ( dateInput, currentYear, currentMonth, CurrentDate) => {
    const months = [
                        "January",
                        "Feburary",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                   ];

    let monthsList = makeNode({
                'type': 'ul',
                'id': 'monthSelect',
                'className': 'month_select'
            });

    months.forEach(function(month) {
        let classes = 'month';
        if (month == months[currentMonth]) {
            classes += ' active';
        }
        let el = makeNode({
                'type': 'li',
                'className': classes,
                'data': ['month', month],
                'content': month
            });
        el.addEventListener('click', event => {
            removeActive(event);

            let selectMonth = event.target.attributes['data-month'].value;

            toggleClass(event.target, 'active');
            
            updateDate(dateInput, {'month': months.indexOf(selectMonth) + 1});

            let setDate = dateInput.getElementsByTagName('input')[0].value.split('-')
            let daysList = makeDays(dateInput, setDate[0], setDate[1], 1);
            let daySelect = document.getElementById('daySelect');
            daySelect.parentNode.removeChild(daySelect);
            let dateDialog = document.getElementById('dateDialog');
            dateDialog.appendChild( daysList );
        });

        monthsList.appendChild( el );
    });

    return monthsList
}
makeDays = ( dateInput, currentYear, currentMonth, CurrentDate ) => {
    // TODO: highlight current day
    
    let days = [ ...range(1, daysInMonth(currentMonth, currentYear)) ];

    let daysList = makeNode({
                'type': 'ul',
                'id': 'daySelect',
                'className': 'day_select'
            });

    days.forEach(function(day) {
        let classes = 'day';
        if (day == CurrentDate) {
            classes += ' active';
        }
        let el = makeNode({
                'type': 'li',
                'className': classes,
                'data': ['day', day],
                'content': day
            });
        el.addEventListener('click', event => {
            removeActive(event);

            let selectDay = event.target.attributes['data-day'].value;

            toggleClass(event.target, 'active');
            updateDate(dateInput, {'day': selectDay});
        });
        daysList.appendChild( el );
    });

    return daysList
}

makeDateDialog = (datePicker, dateInput, currentYear, currentMonth, CurrentDate) => {

    let dateDialog = makeNode({
            'type': 'div',
            'id': 'dateDialog',
            'className': 'date_dialog'
        });
    dateDialog.appendChild(makeYears( 6, dateInput, currentYear, currentMonth, CurrentDate ));
    dateDialog.appendChild(makeMonths( dateInput, currentYear, currentMonth, CurrentDate ));
    dateDialog.appendChild(makeDays( dateInput, currentYear, currentMonth, CurrentDate ));

    datePicker.appendChild( dateDialog );

}

dateInput.addEventListener('click', event => {
    event.preventDefault();
    if (dateInput.classList.contains('active')) {
        let elem = document.getElementById('dateDialog');
        elem.parentNode.removeChild(elem);
    } else {
        makeDateDialog(datePicker, dateInput, thisYear, thisMonth, thisDate);
    }
    toggleClass(dateInput, 'active');
});

setDate(dateInput, thisYear, thisMonth, thisDate)

const resonableDateInput = document.getElementById('resonableDatePicker');
setDate(resonableDateInput, thisYear, thisMonth, thisDate)
